import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator

import random
import string
from datetime import datetime


class Usuario(AbstractUser):
    username = models.CharField(max_length=20, unique=True)
    nombres = models.CharField(max_length=100)
    primer_apellido = models.CharField(max_length=50)
    segundo_apellido = models.CharField(max_length=50, blank=True, null=True)
    tipo_doc = models.CharField(max_length=20)
    documento = models.CharField(max_length=11)
    terminos = models.BooleanField(default=False)
    es_administrativo = models.BooleanField(default=False, blank=True)
    tipo_administrativo = models.CharField(max_length=100)
    facultad_administrativo = models.CharField(max_length=100)
    es_egresado = models.BooleanField(default=False, blank=True)

    def save(self, *args, **kwargs):
        if not self.id:
            # se usa timestamp + 5 numeros random para generar los ID
            timestamp = str(int(datetime.now().timestamp()))
            random_numbers = "".join(random.choices(string.digits, k=5))
            self.id = timestamp + random_numbers
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"
        db_table = "Usuario"


class InformacionBasicaUsuario(models.Model):
    usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)
    nombres = models.CharField(max_length=100)
    primer_apellido = models.CharField(max_length=50, default = "")
    segundo_apellido = models.CharField(max_length=50, blank=True, null=True)
    documento = models.CharField(max_length=11)
    sexo = models.CharField(max_length=20, default = "")
    grupo_etnico = models.CharField(max_length=50,  default = "")
    pais_nacimiento = models.CharField(max_length=100)
    depa_nacimiento = models.CharField(max_length=100, blank=True, null=True)
    mun_nacimiento = models.CharField(max_length=100, blank=True, null=True)    
    fecha_nacimiento = models.DateField(blank=True, null=True)
    estado_civil = models.CharField(max_length=50, default = "")
    discapacidad = models.CharField(max_length=100, blank=True, null=True)
    pais_res = models.CharField(max_length=100, default = "")
    depa_res = models.CharField(max_length=100, blank=True, null=True)
    mun_res = models.CharField(max_length=100, blank=True, null=True)
    direccion_residencia = models.CharField(max_length=100, blank=True)
    correo = models.EmailField()    
    correo_alternativo = models.EmailField(blank=True, null=True)
    telefono_principal = models.CharField(max_length=10, blank=True)
    telefono_alternativo = models.CharField(max_length=17, blank=True, null=True)

    def __str__(self):
        return f"{self.nombres} {self.primer_apellido}"  # Create your models here.

    class Meta:
        verbose_name = "Info Basica"
        verbose_name_plural = "Compilado Info"
        db_table = "Info Basica"


class ProgramasUPN(models.Model):
    programa = models.CharField(max_length=100)
    tipo_formacion = models.CharField(max_length=100)
    facultad = models.CharField(max_length=100)  # Nombre de la facultad
    titulo_obtenido = models.CharField(max_length=100)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)  # Título que se obtiene al completar el programa

    def __str__(self):
        return f"{self.programa} - {self.titulo_obtenido}"

    class Meta:
        verbose_name = "Programa UPN"
        verbose_name_plural = "Programas UPN"
        db_table = "ProgramasUPN"


class UsuarioProgramasUPN(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    programa_upn = models.ForeignKey(ProgramasUPN, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Usuario Programa UPN"
        verbose_name_plural = "Usuarios Programas UPN"
        db_table = "UsuarioProgramasUPN"


class ProgramasExternos(models.Model):
    tipo_titulo = models.CharField(max_length=100)
    programa_externo = models.CharField(max_length=100)
    institucion = models.CharField(max_length=100)  # Nombre de la institución
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)

    def __str__(self):
        return f"{self.nombre_programa} - {self.tipo_titulo} - {self.institucion}"

    class Meta:
        verbose_name = "Programa Externo"
        verbose_name_plural = "Programas Externos"
        db_table = "ProgramasExternos"


class FormacionAcademica(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    programa_upn = models.ForeignKey(
        ProgramasUPN, on_delete=models.CASCADE, null=True, blank=True
    )
    programa_externo = models.ForeignKey(
        ProgramasExternos, on_delete=models.CASCADE, null=True, blank=True
    )
    fecha_inicio = models.DateField(blank=True, null=True)
    fecha_fin = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name = "Formación Académica"
        verbose_name_plural = "Formaciones Académicas"
        db_table = "FormacionAcademica"

class ExperienciaLaboral(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha_inicio = models.DateField()
    fecha_finalizacion = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True

class Docencia(ExperienciaLaboral):
    TIPO_INSTITUCION_CHOICES = [
        ('PUB', 'Pública'),
        ('PRI', 'Privada'),
        ('MIX', 'Mixta'),
    ]

    nombre_institucion = models.CharField(max_length=100)
    tipo_institucion = models.CharField(max_length=3, choices=TIPO_INSTITUCION_CHOICES)

class Cargo(models.Model):
    CARGO_CHOICES = [
        ('DIR', 'Directivo'),
        ('REC', 'Rector'),
        ('COO', 'Coordinador'),
        ('DOC', 'Docencia'),
        ('DIG', 'Dirección de grupo'),
        ('JEF', 'Jefatura de área'),
        ('CAP', 'Capacitación'),
        ('ASE', 'Asesoría pedagógica'),
        ('GES', 'Gestión de calidad'),
        ('DIS', 'Diseño de materiales'),
        ('INV', 'Investigación'),
        ('PAR', 'Participación en proyectos transversales'),
        ('NIV', 'Nivelación'),
        ('OTR', 'Otro'),
    ]

    docencia = models.ForeignKey(Docencia, on_delete=models.CASCADE)
    cargo = models.CharField(max_length=3, choices=CARGO_CHOICES)

class Editorial(ExperienciaLaboral):
    empresa_editorial = models.CharField(max_length=100)

class AsesorPedagogico(ExperienciaLaboral):
    institucion = models.CharField(max_length=100)

class ClasesParticulares(ExperienciaLaboral):
    pass

class DatosEmpleador(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    institucion = models.CharField(max_length=100)
    direccion_institucion = models.CharField(max_length=200)
    correo = models.EmailField()
    telefono_institucional = models.CharField(max_length=20)
    percepcion_egresado = models.TextField()

    def __str__(self):
        return self.nombre
    
class ProductividadAcademica(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)

class Meta:
    abstract = True

class Libro(ProductividadAcademica):
    titulo = models.CharField(max_length=200)
    anio_publicacion = models.IntegerField()
    editorial = models.CharField(max_length=100)

class TextoEducativo(ProductividadAcademica):
    titulo = models.CharField(max_length=200)
    anio_publicacion = models.IntegerField()
    editorial = models.CharField(max_length=100)

class Articulo(ProductividadAcademica):
    titulo = models.CharField(max_length=200)
    anio_publicacion = models.IntegerField()
    revista = models.CharField(max_length=100)

class PonenciaEnMemoria(ProductividadAcademica):
    titulo = models.CharField(max_length=200)
    anio_publicacion = models.IntegerField()
    evento = models.CharField(max_length=100)
    ciudad_pais = models.CharField(max_length=100)

class AsistenciaEventosAcademicos(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    nombre_evento = models.CharField(max_length=200)
    anio = models.IntegerField()
    tipo_participacion = models.CharField(max_length=50)
    ambito = models.CharField(max_length=50)
    organizadores = models.CharField(max_length=200)

class Investigacion(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    titulo_proyecto = models.CharField(max_length=200)
    anio_inicio = models.IntegerField()
    anio_finalizacion = models.IntegerField()
    grupo_investigacion = models.CharField(max_length=100)
    entidad_financiadora = models.CharField(max_length=100)

class ReconocimientosAcademicos(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    tipo_reconocimiento = models.CharField(max_length=100)
    quien_otorga = models.CharField(max_length=100)
    anio = models.IntegerField()

class ParticipacionEnRedes(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    nombre_red = models.CharField(max_length=100)
    anio_inicio = models.IntegerField()
    anio_finalizacion = models.IntegerField()

class ParticipacionEnOrganizaciones(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    nombre_organizacion = models.CharField(max_length=100)
    tipo_organizacion = models.CharField(max_length=50)
    funcion_organizacion = models.CharField(max_length=100)
    anio_inicio = models.IntegerField()
    anio_finalizacion = models.IntegerField()