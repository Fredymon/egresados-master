from django.shortcuts import render, redirect, get_object_or_404
from .models import Proyecto, Tarea, Cronograma
from .forms import ProyectoForm, TareaForm, CronogramaForm

def lista_proyectos(request):
    proyectos = Proyecto.objects.all()
    return render(request, 'proyectos/lista_proyectos.html', {'proyectos': proyectos})

def detalle_proyecto(request, proyecto_id):
    proyecto = get_object_or_404(Proyecto, id=proyecto_id)
    return render(request, 'proyectos/detalle_proyecto.html', {'proyecto': proyecto})

def crear_proyecto(request):
    if request.method == 'POST':
        form = ProyectoForm(request.POST)
        if form.is_valid():
            proyecto = form.save(commit=False)
            proyecto.administrador = request.user
            proyecto.save()
            return redirect('lista_proyectos')
    else:
        form = ProyectoForm()
    return render(request, 'proyectos/crear_proyecto.html', {'form': form})

def agregar_miembro(request, proyecto_id):
    proyecto = get_object_or_404(Proyecto, id=proyecto_id)
    if request.method == 'POST':
        usuario_id = request.POST.get('usuario_id')
        usuario = get_object_or_404(User, id=usuario_id)
        proyecto.miembros.add(usuario)
        return redirect('detalle_proyecto', proyecto_id=proyecto.id)
    return render(request, 'proyectos/agregar_miembro.html', {'proyecto': proyecto})

def crear_tarea(request, proyecto_id):
    proyecto = get_object_or_404(Proyecto, id=proyecto_id)
    if request.method == 'POST':
        form = TareaForm(request.POST)
        if form.is_valid():
            tarea = form.save(commit=False)
            tarea.proyecto = proyecto
            tarea.save()
            return redirect('detalle_proyecto', proyecto_id=proyecto.id)
    else:
        form = TareaForm()
    return render(request, 'proyectos/crear_tarea.html', {'form': form, 'proyecto': proyecto})

def crear_cronograma(request, proyecto_id):
    proyecto = get_object_or_404(Proyecto, id=proyecto_id)
    if request.method == 'POST':
        form = CronogramaForm(request.POST)
        if form.is_valid():
            cronograma = form.save(commit=False)
            cronograma.proyecto = proyecto
            cronograma.save()
            return redirect('detalle_proyecto', proyecto_id=proyecto.id)
    else:
        form = CronogramaForm()
    return render(request, 'proyectos/crear_cronograma.html', {'form': form, 'proyecto': proyecto})

# Create your views here.
