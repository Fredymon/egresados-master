from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Proyecto(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    tipo_proyecto = models.CharField(max_length=50)
    administrador = models.ForeignKey(User, on_delete=models.CASCADE, related_name='proyectos_administrados')
    miembros = models.ManyToManyField(User, related_name='proyectos_miembro')

class Tarea(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name='tareas')
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()
    asignado_a = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tareas_asignadas')

class Cronograma(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name='cronogramas')
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField()
    fecha_inicio = models.DateField()
    fecha_fin = models.DateField()

# Create your models here.
