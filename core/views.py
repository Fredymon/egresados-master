from django.shortcuts import render, redirect
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.forms import AuthenticationForm
from django.template import Context
from django.contrib import messages
from django.http import JsonResponse
from .forms import *
from .models import *
from django.core.cache import cache
import csv


def home(request):
    form = InfoBasicForm()
    return render(request, "core/home.html", {"form": form})


def registro(request):
    url_name = request.resolver_match.url_name
    print(url_name)
    # return_url = "core/registro-egresado.html" if url_name == "registro-egresado" else "core/registro-administrativo.html"
    if request.method == "POST":
        form = RegistroFormulario(request.POST, url_name=url_name)
        if form.is_valid():
            print("Formulario Válido")
            usuario = form.save(commit=False)
            usuario.username = usuario.username.lower()
            if url_name == "registro-egresado":
                usuario.es_egresado = True
            else:
                usuario.es_administrativo = True
                form.fields['tipo_administrativo'].required = usuario.es_administrativo
                form.fields['facultad_administrativo'].required = usuario.es_administrativo
            usuario.save()
            auth_login(request, usuario)

            return redirect("perfil", id=request.user.id)  # redireccion
        else:
    
            print("Formulario Inválido")
            for field in form.errors:
                for error in form[field].errors:
                    print(f"Error en campo '{field}': {error}")
                    messages.error(request, f"Error en campo '{field}': {error}")

    else:
        form = RegistroFormulario(url_name=url_name)

    return render(request, "core/" + url_name + ".html", {"registerForm": form, "url_name": url_name})


def inicio(request):
    if request.method == "POST":
        form = CustomAuthenticationForm(request, data=request.POST)
        if form.is_valid():
            print(" Formulario Válido. ")
            auth_login(request, form.get_user())
            return redirect("perfil", id=request.user.id)
        else:
            messages.error(request, "Verifique sus credenciales")
    else:
        form = CustomAuthenticationForm()
    return render(request, "core/login.html", {"loginForm": form})


def perfil(request, id):
    usuario = get_object_or_404(Usuario, pk=id)
    return render(request, "core/perfil.html", {"usuario": usuario})

def obtener_datos_api():
    url = 'https://www.datos.gov.co/resource/gdxc-w37w.json'
    try:
        respuesta = requests.get(url)
        if respuesta.status_code == 200:
            departamentos, municipios = procesar_datos_api(respuesta.json())
            return departamentos, municipios
    except requests.RequestException:
        pass
    return [], []

def procesar_datos_api(datos):
    departamentos = set()
    municipios = set()
    for item in datos:
        departamentos.add(item['dpto'])
        municipios.add(item['nom_mpio'])
    return list(departamentos), list(municipios)

def leer_datos_csv(ruta_archivo):
    # Claves de caché
    cache_key_departamentos = 'departamentos_data'
    cache_key_municipios = 'municipios_data'

    # Intenta obtener los datos de la caché
    departamentos = cache.get(cache_key_departamentos)
    municipios = cache.get(cache_key_municipios)

    # Si no están en caché, lee del archivo CSV y almacena en caché
    if departamentos is None or municipios is None:
        departamentos = set()
        municipios = set()
        with open(ruta_archivo, mode='r', encoding='utf-8') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                departamentos.add(row['Nombre Departamento'])
                municipios.add(row['Nombre Municipio'])
        departamentos = sorted(departamentos)
        municipios = sorted(municipios)
        cache.set(cache_key_departamentos, departamentos)
        cache.set(cache_key_municipios, municipios)

    return departamentos, municipios

def cargar_municipios(request):
    departamento = request.GET.get('departamento')
    departamentos_municipios = leer_datos_csv('ruta/a/tu/archivo.csv')
    municipios = departamentos_municipios.get(departamento, [])
    return JsonResponse(municipios, safe=False)

def infoBasica(request, id):
    usuario = get_object_or_404(Usuario, pk=id)

    try:
        info_basica = InformacionBasicaUsuario.objects.get(usuario=usuario)
    except InformacionBasicaUsuario.DoesNotExist:
        info_basica = None

    ruta_csv = 'core/static/data/DIVIPOLA-_C_digos_municipios_20231227.csv'
    departamentos, municipios = leer_datos_csv(ruta_csv)

    if request.method == "POST":
        form = InfoBasicForm(request.POST, departamentos=departamentos, municipios=municipios, instance=info_basica)
        if info_basica is None:
            info_basica = form.save(commit=False)
            info_basica.usuario = usuario

        if form.is_valid():
            form.save()
            return render(request, "core/info-basica.html", {"usuario": usuario, "form": form, "form_saved": True})
        else:
            return render(request, "core/info-basica.html", {"usuario": usuario, "form": form, "form_errors": True})
    else:
        data_inicial = {
            "nombres": usuario.nombres,
            "primer_apellido": usuario.primer_apellido,
            "segundo_apellido": usuario.segundo_apellido,
            "documento": usuario.documento,
            "correo": usuario.email,
            
        }
        if info_basica: 
            info_basica.fecha_nacimiento = str(info_basica.fecha_nacimiento)
            
        form = InfoBasicForm(initial=data_inicial, departamentos=departamentos, municipios=municipios, instance=info_basica)

    return render(request, "core/info-basica.html", {"usuario": usuario, "form": form})

def formacionUPN(request):
    
    form = ProgramasUPNForm(request.POST)

    if request.method == "POST" and form.is_valid():
        form.save()
        return redirect('formacionUPN')  # Reemplaza con tu URL de redirección

    return render(request, "core/formacionUPN.html", {"form": form})

def salir(request):
    auth_logout(request)
    return render(request, "core/salir.html")


def get_programas(request):
    facultad = request.GET.get('facultad')
    tipo_formacion = request.GET.get('tipo_formacion')

    # Filtrar los programas basados en la facultad y el tipo de formación
    programas_filtrados = Programa.objects.filter(
        facultad=facultad, tipo_formacion=tipo_formacion
    )

    # Crear una lista de tuplas (value, text) para la respuesta JSON
    programas = [(programa.id, programa.nombre) for programa in programas_filtrados]

    return JsonResponse({'programas': programas})

def experiencia_laboral(request, usuario_id):
    usuario = get_object_or_404(Usuario, id=usuario_id)
    docencias = Docencia.objects.filter(usuario=usuario)
    editoriales = Editorial.objects.filter(usuario=usuario)
    asesores_pedagogicos = AsesorPedagogico.objects.filter(usuario=usuario)
    clases_particulares = ClasesParticulares.objects.filter(usuario=usuario)

    if request.method == 'POST':
        form_docencia = DocenciaForm(request.POST)
        form_cargo = CargoForm(request.POST)
        form_editorial = EditorialForm(request.POST)
        form_asesor_pedagogico = AsesorPedagogicoForm(request.POST)
        form_clases_particulares = ClasesParticularesForm(request.POST)

        if form_docencia.is_valid():
            docencia = form_docencia.save(commit=False)
            docencia.usuario = usuario
            docencia.save()
            return redirect('experiencia_laboral', usuario_id=usuario.id)

        if form_cargo.is_valid():
            cargo = form_cargo.save(commit=False)
            cargo.docencia = Docencia.objects.latest('id')
            cargo.save()
            return redirect('experiencia_laboral', usuario_id=usuario.id)

        if form_editorial.is_valid():
            editorial = form_editorial.save(commit=False)
            editorial.usuario = usuario
            editorial.save()
            return redirect('experiencia_laboral', usuario_id=usuario.id)

        if form_asesor_pedagogico.is_valid():
            asesor_pedagogico = form_asesor_pedagogico.save(commit=False)
            asesor_pedagogico.usuario = usuario
            asesor_pedagogico.save()
            return redirect('experiencia_laboral', usuario_id=usuario.id)

        if form_clases_particulares.is_valid():
            clases_particulares = form_clases_particulares.save(commit=False)
            clases_particulares.usuario = usuario
            clases_particulares.save()
            return redirect('experiencia_laboral', usuario_id=usuario.id)
    else:
        form_docencia = DocenciaForm()
        form_cargo = CargoForm()
        form_editorial = EditorialForm()
        form_asesor_pedagogico = AsesorPedagogicoForm()
        form_clases_particulares = ClasesParticularesForm()

    context = {
        'usuario': usuario,
        'docencias': docencias,
        'editoriales': editoriales,
        'asesores_pedagogicos': asesores_pedagogicos,
        'clases_particulares': clases_particulares,
        'form_docencia': form_docencia,
        'form_cargo': form_cargo,
        'form_editorial': form_editorial,
        'form_asesor_pedagogico': form_asesor_pedagogico,
        'form_clases_particulares': form_clases_particulares,
    }

    return render(request, 'experiencia_laboral.html', context)

# class DatosEmpleadorForm(forms.ModelForm):
#     class Meta:
#         model = DatosEmpleador
#         fields = ['nombre', 'institucion', 'direccion_institucion', 'correo', 'telefono_institucional', 'percepcion_egresado']
#         labels = {
#             'nombre': 'Nombre',
#             'institucion': 'Institución',
#             'direccion_institucion': 'Dirección institución',
#             'correo': 'Correo',
#             'telefono_institucional': 'Teléfono institucional',
#             'percepcion_egresado': 'Percepción del egresado',
#         }
#         widgets = {
#             'nombre': forms.TextInput(attrs={'class': 'form-control'}),
#             'institucion': forms.TextInput(attrs={'class': 'form-control'}),
#             'direccion_institucion': forms.TextInput(attrs={'class': 'form-control'}),
#             'correo': forms.EmailInput(attrs={'class': 'form-control'}),
#             'telefono_institucional': forms.TextInput(attrs={'class': 'form-control'}),
#             'percepcion_egresado': forms.Textarea(attrs={'class': 'form-control'}),
#         }

def productividad_academica(request, usuario_id):
    usuario = get_object_or_404(Usuario, id=usuario_id)
    libros = Libro.objects.filter(usuario=usuario)
    textos_educativos = TextoEducativo.objects.filter(usuario=usuario)
    articulos = Articulo.objects.filter(usuario=usuario)
    ponencias_en_memoria = PonenciaEnMemoria.objects.filter(usuario=usuario)

    if request.method == 'POST':
        form_libro = LibroForm(request.POST)
        form_texto_educativo = TextoEducativoForm(request.POST)
        form_articulo = ArticuloForm(request.POST)
        form_ponencia_en_memoria = PonenciaEnMemoriaForm(request.POST)

        if form_libro.is_valid():
            libro = form_libro.save(commit=False)
            libro.usuario = usuario
            libro.save()
            return redirect('productividad_academica', usuario_id=usuario.id)

        if form_texto_educativo.is_valid():
            texto_educativo = form_texto_educativo.save(commit=False)
            texto_educativo.usuario = usuario
            texto_educativo.save()
            return redirect('productividad_academica', usuario_id=usuario.id)

        if form_articulo.is_valid():
            articulo = form_articulo.save(commit=False)
            articulo.usuario = usuario
            articulo.save()
            return redirect('productividad_academica', usuario_id=usuario.id)

        if form_ponencia_en_memoria.is_valid():
            ponencia_en_memoria = form_ponencia_en_memoria.save(commit=False)
            ponencia_en_memoria.usuario = usuario
            ponencia_en_memoria.save()
            return redirect('productividad_academica', usuario_id=usuario.id)
    else:
        form_libro = LibroForm()
        form_texto_educativo = TextoEducativoForm()
        form_articulo = ArticuloForm()
        form_ponencia_en_memoria = PonenciaEnMemoriaForm()

    context = {
        'usuario': usuario,
        'libros': libros,
        'textos_educativos': textos_educativos,
        'articulos': articulos,
        'ponencias_en_memoria': ponencias_en_memoria,
        'form_libro': form_libro,
        'form_texto_educativo': form_texto_educativo,
        'form_articulo': form_articulo,
        'form_ponencia_en_memoria': form_ponencia_en_memoria,
    }

    return render(request, 'productividad_academica.html', context)

def asistencia_eventos_academicos(request, usuario_id):
    usuario = get_object_or_404(Usuario, id=usuario_id)
    asistencias = AsistenciaEventosAcademicos.objects.filter(usuario=usuario)

    if request.method == 'POST':
        form = AsistenciaEventosAcademicosForm(request.POST)
        if form.is_valid():
            asistencia = form.save(commit=False)
            asistencia.usuario = usuario
            asistencia.save()
            return redirect('asistencia_eventos_academicos', usuario_id=usuario.id)
    else:
        form = AsistenciaEventosAcademicosForm()

    context = {
        'usuario': usuario,
        'asistencias': asistencias,
        'form': form,
    }

    return render(request, 'asistencia_eventos_academicos.html', context)

def investigacion(request, usuario_id):
    usuario = get_object_or_404(Usuario, id=usuario_id)
    investigaciones = Investigacion.objects.filter(usuario=usuario)

    if request.method == 'POST':
        form = InvestigacionForm(request.POST)
        if form.is_valid():
            investigacion = form.save(commit=False)
            investigacion.usuario = usuario
            investigacion.save()
            return redirect('investigacion', usuario_id=usuario.id)
    else:
        form = InvestigacionForm()

    context = {
        'usuario': usuario,
        'investigaciones': investigaciones,
        'form': form,
    }

    return render(request, 'investigacion.html', context)

def reconocimientos_academicos(request, usuario_id):
    usuario = get_object_or_404(Usuario, id=usuario_id)
    reconocimientos = ReconocimientosAcademicos.objects.filter(usuario=usuario)

    if request.method == 'POST':
        form = ReconocimientosAcademicosForm(request.POST)
        if form.is_valid():
            reconocimiento = form.save(commit=False)
            reconocimiento.usuario = usuario
            reconocimiento.save()
            return redirect('reconocimientos_academicos', usuario_id=usuario.id)
    else:
        form = ReconocimientosAcademicosForm()

    context = {
        'usuario': usuario,
        'reconocimientos': reconocimientos,
        'form': form,
    }

    return render(request, 'reconocimientos_academicos.html', context)

def participacion_redes(request, usuario_id):
    usuario = get_object_or_404(Usuario, id=usuario_id)
    participaciones_redes = ParticipacionEnRedes.objects.filter(usuario=usuario)

    if request.method == 'POST':
        form = ParticipacionEnRedesForm(request.POST)
        if form.is_valid():
            participacion = form.save(commit=False)
            participacion.usuario = usuario
            participacion.save()
            return redirect('participacion_redes', usuario_id=usuario.id)
    else:
        form = ParticipacionEnRedesForm()

    context = {
        'usuario': usuario,
        'participaciones_redes': participaciones_redes,
        'form': form,
    }

    return render(request, 'participacion_redes.html', context)

def participacion_organizaciones(request, usuario_id):
    usuario = get_object_or_404(Usuario, id=usuario_id)
    participaciones_organizaciones = ParticipacionEnOrganizaciones.objects.filter(usuario=usuario)

    if request.method == 'POST':
        form = ParticipacionEnOrganizacionesForm(request.POST)
        if form.is_valid():
            participacion = form.save(commit=False)
            participacion.usuario = usuario
            participacion.save()
            return redirect('participacion_organizaciones', usuario_id=usuario.id)
    else:
        form = ParticipacionEnOrganizacionesForm()

    context = {
        'usuario': usuario,
        'participaciones_organizaciones': participaciones_organizaciones,
        'form': form,
    }

    return render(request, 'participacion_organizaciones.html', context)