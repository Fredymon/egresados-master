from django.apps import AppConfig


class SearchmoduleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'searchModule'
