from django.shortcuts import render
from .forms import BusquedaEgresadoForm
from .models import Usuario, InformacionBasicaUsuario, ProgramasUPN, ProgramasExternos, FormacionAcademica, ExperienciaLaboral, Docencia, Editorial, AsesorPedagogico, DatosEmpleador, ProductividadAcademica, Libro, TextoEducativo, Articulo, PonenciaEnMemoria, AsistenciaEventosAcademicos, Investigacion, ReconocimientosAcademicos, ParticipacionEnRedes, ParticipacionEnOrganizaciones

from  core.models import *

def buscar_egresados(request):
    form = BusquedaEgresadoForm(request.GET or None)
    egresados = Usuario.objects.filter(es_egresado=True)

    if form.is_valid():
        nombres = form.cleaned_data.get('nombres')
        primer_apellido = form.cleaned_data.get('primer_apellido')
        segundo_apellido = form.cleaned_data.get('segundo_apellido')
        documento = form.cleaned_data.get('documento')
        programa_upn = form.cleaned_data.get('programa_upn')
        programa_externo = form.cleaned_data.get('programa_externo')
        facultad = form.cleaned_data.get('facultad')
        titulo_obtenido = form.cleaned_data.get('titulo_obtenido')
        nombre_institucion_docencia = form.cleaned_data.get('nombre_institucion_docencia')
        empresa_editorial = form.cleaned_data.get('empresa_editorial')
        institucion_asesor = form.cleaned_data.get('institucion_asesor')
        nombre_empleador = form.cleaned_data.get('nombre_empleador')
        institucion_empleador = form.cleaned_data.get('institucion_empleador')
        titulo_libro = form.cleaned_data.get('titulo_libro')
        titulo_texto_educativo = form.cleaned_data.get('titulo_texto_educativo')
        titulo_articulo = form.cleaned_data.get('titulo_articulo')
        titulo_ponencia = form.cleaned_data.get('titulo_ponencia')
        nombre_evento = form.cleaned_data.get('nombre_evento')
        titulo_proyecto_investigacion = form.cleaned_data.get('titulo_proyecto_investigacion')
        grupo_investigacion = form.cleaned_data.get('grupo_investigacion')
        tipo_reconocimiento = form.cleaned_data.get('tipo_reconocimiento')
        nombre_red = form.cleaned_data.get('nombre_red')
        nombre_organizacion = form.cleaned_data.get('nombre_organizacion')

        if nombres:
            egresados = egresados.filter(informacionbasicausuario__nombres__icontains=nombres)
        if primer_apellido:
            egresados = egresados.filter(informacionbasicausuario__primer_apellido__icontains=primer_apellido)
        if segundo_apellido:
            egresados = egresados.filter(informacionbasicausuario__segundo_apellido__icontains=segundo_apellido)
        if documento:
            egresados = egresados.filter(informacionbasicausuario__documento__icontains=documento)
        if programa_upn:
            egresados = egresados.filter(formacionacademica__programa_upn=programa_upn)
        if programa_externo:
            egresados = egresados.filter(formacionacademica__programa_externo=programa_externo)
        if facultad:
            egresados = egresados.filter(formacionacademica__programa_upn__facultad__icontains=facultad)
        if titulo_obtenido:
            egresados = egresados.filter(formacionacademica__programa_upn__titulo_obtenido__icontains=titulo_obtenido)
        if nombre_institucion_docencia:
            egresados = egresados.filter(docencia__nombre_institucion__icontains=nombre_institucion_docencia)
        if empresa_editorial:
            egresados = egresados.filter(editorial__empresa_editorial__icontains=empresa_editorial)
        if institucion_asesor:
            egresados = egresados.filter(asesorpedagogico__institucion__icontains=institucion_asesor)
        if nombre_empleador:
            egresados = egresados.filter(datosempleador__nombre__icontains=nombre_empleador)
        if institucion_empleador:
            egresados = egresados.filter(datosempleador__institucion__icontains=institucion_empleador)
        if titulo_libro:
            egresados = egresados.filter(libro__titulo__icontains=titulo_libro)
        if titulo_texto_educativo:
            egresados = egresados.filter(textoeducativo__titulo__icontains=titulo_texto_educativo)
        if titulo_articulo:
            egresados = egresados.filter(articulo__titulo__icontains=titulo_articulo)
        if titulo_ponencia:
            egresados = egresados.filter(ponenciaenmemoria__titulo__icontains=titulo_ponencia)
        if nombre_evento:
            egresados = egresados.filter(asistenciaeventosacademicos__nombre_evento__icontains=nombre_evento)
        if titulo_proyecto_investigacion:
            egresados = egresados.filter(investigacion__titulo_proyecto__icontains=titulo_proyecto_investigacion)
        if grupo_investigacion:
            egresados = egresados.filter(investigacion__grupo_investigacion__icontains=grupo_investigacion)
        if tipo_reconocimiento:
            egresados = egresados.filter(reconocimientosacademicos__tipo_reconocimiento__icontains=tipo_reconocimiento)
        if nombre_red:
            egresados = egresados.filter(participacionenredes__nombre_red__icontains=nombre_red)
        if nombre_organizacion:
            egresados = egresados.filter(participacionenorganizaciones__nombre_organizacion__icontains=nombre_organizacion)

    context = {
        'form': form,
        'egresados': egresados,
    }

    return render(request, 'buscar_egresados.html', context)

def buscar_egresados_by_form(request):
    form = BusquedaEgresadoForm(request.GET or None)
    egresados = Usuario.objects.filter(es_egresado=True)

    search_fields = {
        'nombres': 'informacionbasicausuario__nombres__icontains',
        'primer_apellido': 'informacionbasicausuario__primer_apellido__icontains',
        'segundo_apellido': 'informacionbasicausuario__segundo_apellido__icontains',
        'documento': 'informacionbasicausuario__documento__icontains',
        'programa_upn': 'formacionacademica__programa_upn',
        'programa_externo': 'formacionacademica__programa_externo',
        'facultad': 'formacionacademica__programa_upn__facultad__icontains',
        'titulo_obtenido': 'formacionacademica__programa_upn__titulo_obtenido__icontains',
        'fecha_fin': 'formacionacademica__fecha_fin',
        'nombre_institucion_docencia': 'docencia__nombre_institucion__icontains',
        'tipo_institucion_docencia': 'docencia__tipo_institucion',
        'cargo_docencia': 'cargo__cargo',
        'empresa_editorial': 'editorial__empresa_editorial__icontains',
        'institucion_asesor': 'asesorpedagogico__institucion__icontains',
        'nombre_empleador': 'datosempleador__nombre__icontains',
        'institucion_empleador': 'datosempleador__institucion__icontains',
        'titulo_libro': 'libro__titulo__icontains',
        'titulo_texto_educativo': 'textoeducativo__titulo__icontains',
        'titulo_articulo': 'articulo__titulo__icontains',
        'titulo_ponencia': 'ponenciaenmemoria__titulo__icontains',
        'nombre_evento': 'asistenciaeventosacademicos__nombre_evento__icontains',
        'tipo_participacion_evento': 'asistenciaeventosacademicos__tipo_participacion__icontains',
        'ambito_evento': 'asistenciaeventosacademicos__ambito__icontains',
        'titulo_proyecto_investigacion': 'investigacion__titulo_proyecto__icontains',
        'grupo_investigacion': 'investigacion__grupo_investigacion__icontains',
        'tipo_reconocimiento': 'reconocimientosacademicos__tipo_reconocimiento__icontains',
        'quien_otorga_reconocimiento': 'reconocimientosacademicos__quien_otorga__icontains',
        'nombre_red': 'participacionenredes__nombre_red__icontains',
        'nombre_organizacion': 'participacionenorganizaciones__nombre_organizacion__icontains',
        'tipo_organizacion': 'participacionenorganizaciones__tipo_organizacion__icontains',
        'funcion_organizacion': 'participacionenorganizaciones__funcion_organizacion__icontains',
    }

    if form.is_valid():
        for field, lookup in search_fields.items():
            value = form.cleaned_data.get(field)
            if value:
                egresados = egresados.filter(**{lookup: value})

    context = {
        'form': form,
        'egresados': egresados,
    }

    return render(request, 'buscar_egresados.html', context)
