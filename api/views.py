from rest_framework import generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from core.models import Usuario, InformacionBasicaUsuario, ProgramasUPN, ProgramasExternos, FormacionAcademica
from .serializers import UsuarioSerializer, InformacionBasicaUsuarioSerializer, ProgramasUPNSerializer, ProgramasExternosSerializer, FormacionAcademicaSerializer
from rest_framework import DjangoFilterBackend, FilterSet
from rest_framework import filters
from core.models import *


class UsuarioList(generics.ListCreateAPIView):
    queryset = Usuario.objects.filter(es_egresado=True)
    serializer_class = UsuarioSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class UsuarioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Usuario.objects.filter(es_egresado=True)
    serializer_class = UsuarioSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class InformacionBasicaUsuarioList(generics.ListCreateAPIView):
    queryset = InformacionBasicaUsuario.objects.all()
    serializer_class = InformacionBasicaUsuarioSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class InformacionBasicaUsuarioDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = InformacionBasicaUsuario.objects.all()
    serializer_class = InformacionBasicaUsuarioSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class ProgramasUPNList(generics.ListCreateAPIView):
    queryset = ProgramasUPN.objects.all()
    serializer_class = ProgramasUPNSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class ProgramasUPNDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProgramasUPN.objects.all()
    serializer_class = ProgramasUPNSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class ProgramasExternosList(generics.ListCreateAPIView):
    queryset = ProgramasExternos.objects.all()
    serializer_class = ProgramasExternosSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class ProgramasExternosDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProgramasExternos.objects.all()
    serializer_class = ProgramasExternosSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class FormacionAcademicaList(generics.ListCreateAPIView):
    queryset = FormacionAcademica.objects.all()
    serializer_class = FormacionAcademicaSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class FormacionAcademicaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = FormacionAcademica.objects.all()
    serializer_class = FormacionAcademicaSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

class UsuarioFilter(Filterset):
    class Meta:
        model = Usuario
        fields = ['nombres', 'primer_apellido', 'segundo_apellido', 'tipo_doc', 'documento']

# Autenticación API por Token

# class UsuarioList(generics.ListCreateAPIView):
#     queryset = Usuario.objects.filter(es_egresado=True)
#     serializer_class = UsuarioSerializer
#     authentication_classes = [TokenAuthentication]
#     permission_classes = [IsAuthenticated]
#     filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
#     filterset_class = UsuarioFilter
#     ordering_fields = ['nombres', 'primer_apellido', 'segundo_apellido']