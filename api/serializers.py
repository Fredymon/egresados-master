from rest_framework import serializers
from core.models import Usuario, InformacionBasicaUsuario, ProgramasUPN, ProgramasExternos, FormacionAcademica, ExperienciaLaboral, Docencia, Editorial, AsesorPedagogico, DatosEmpleador, ProductividadAcademica, Libro, TextoEducativo, Articulo, PonenciaEnMemoria, AsistenciaEventosAcademicos, Investigacion, ReconocimientosAcademicos, ParticipacionEnRedes, ParticipacionEnOrganizaciones

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ['id', 'username', 'nombres', 'primer_apellido', 'segundo_apellido', 'tipo_doc', 'documento', 'es_egresado']

class InformacionBasicaUsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = InformacionBasicaUsuario
        fields = ['usuario', 'nombres', 'primer_apellido', 'segundo_apellido', 'documento', 'sexo', 'grupo_etnico', 'pais_nacimiento', 'depa_nacimiento', 'mun_nacimiento', 'fecha_nacimiento', 'estado_civil', 'discapacidad', 'pais_res', 'depa_res', 'mun_res', 'direccion_residencia', 'correo', 'correo_alternativo', 'telefono_principal', 'telefono_alternativo']

class ProgramasUPNSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProgramasUPN
        fields = ['id', 'programa', 'tipo_formacion', 'facultad', 'titulo_obtenido', 'fecha_inicio', 'fecha_fin']

class ProgramasExternosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProgramasExternos
        fields = ['id', 'tipo_titulo', 'programa_externo', 'institucion', 'usuario', 'fecha_inicio', 'fecha_fin']

class FormacionAcademicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormacionAcademica
        fields = ['id', 'usuario', 'programa_upn', 'programa_externo', 'fecha_inicio', 'fecha_fin']

class ExperienciaLaboralSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExperienciaLaboral
        fields = ['id', 'usuario', 'fecha_inicio', 'fecha_finalizacion']

class DocenciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Docencia
        fields = ['id', 'usuario', 'fecha_inicio', 'fecha_finalizacion', 'nombre_institucion', 'tipo_institucion']

class EditorialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Editorial
        fields = ['id', 'usuario', 'fecha_inicio', 'fecha_finalizacion', 'empresa_editorial']

class AsesorPedagogicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AsesorPedagogico
        fields = ['id', 'usuario', 'fecha_inicio', 'fecha_finalizacion', 'institucion']

class DatosEmpleadorSerializer(serializers.ModelSerializer):
    class Meta:
        model = DatosEmpleador
        fields = ['id', 'usuario', 'nombre', 'institucion', 'direccion_institucion', 'correo', 'telefono_institucional', 'percepcion_egresado']

class ProductividadAcademicaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductividadAcademica
        fields = ['id', 'usuario']

class LibroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Libro
        fields = ['id', 'usuario', 'titulo', 'anio_publicacion', 'editorial']

class TextoEducativoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextoEducativo
        fields = ['id', 'usuario', 'titulo', 'anio_publicacion', 'editorial']

class ArticuloSerializer(serializers.ModelSerializer):
    class Meta:
        model = Articulo
        fields = ['id', 'usuario', 'titulo', 'anio_publicacion', 'revista']

class PonenciaEnMemoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PonenciaEnMemoria
        fields = ['id', 'usuario', 'titulo', 'anio_publicacion', 'evento', 'ciudad_pais']

class AsistenciaEventosAcademicosSerializer(serializers.ModelSerializer):
    class Meta:
        model = AsistenciaEventosAcademicos
        fields = ['id', 'usuario', 'nombre_evento', 'anio', 'tipo_participacion', 'ambito', 'organizadores']

class InvestigacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Investigacion
        fields = ['id', 'usuario', 'titulo_proyecto', 'anio_inicio', 'anio_finalizacion', 'grupo_investigacion', 'entidad_financiadora']

class ReconocimientosAcademicosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReconocimientosAcademicos
        fields = ['id', 'usuario', 'tipo_reconocimiento', 'quien_otorga', 'anio']

class ParticipacionEnRedesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParticipacionEnRedes
        fields = ['id', 'usuario', 'nombre_red', 'anio_inicio', 'anio_finalizacion']

class ParticipacionEnOrganizacionesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParticipacionEnOrganizaciones
        fields = ['id', 'usuario', 'nombre_organizacion', 'tipo_organizacion', 'funcion_organizacion', 'anio_inicio', 'anio_finalizacion']