from django.urls import path
from . import views

urlpatterns = [
    path('usuarios/', views.UsuarioList.as_view(), name='usuario-list'),
    path('usuarios/<int:pk>/', views.UsuarioDetail.as_view(), name='usuario-detail'),
    path('informacion-basica/', views.InformacionBasicaUsuarioList.as_view(), name='informacion-basica-list'),
    path('informacion-basica/<int:pk>/', views.InformacionBasicaUsuarioDetail.as_view(), name='informacion-basica-detail'),
    path('programas-upn/', views.ProgramasUPNList.as_view(), name='programas-upn-list'),
    path('programas-upn/<int:pk>/', views.ProgramasUPNDetail.as_view(), name='programas-upn-detail'),
    path('programas-externos/', views.ProgramasExternosList.as_view(), name='programas-externos-list'),
    path('programas-externos/<int:pk>/', views.ProgramasExternosDetail.as_view(), name='programas-externos-detail'),
    path('formacion-academica/', views.FormacionAcademicaList.as_view(), name='formacion-academica-list'),
    path('formacion-academica/<int:pk>/', views.FormacionAcademicaDetail.as_view(), name='formacion-academica-detail'),
]

